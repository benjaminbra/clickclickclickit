var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http),
    userscore = 0,
    red = 0,
    green = 0,
    blue = 0,
    colorBoost = [50, 200, 400, 500, 1000],
    colorBoostBonus = [2, 5, 10, 15, 100],
    multiplicator = 0.5,
    listBoost = [0, 0, 0],
    victory = false,
    redStop = false,
    greenStop = false,
    blueStop = false,
    redCounter = 0,
    greenCounter = 0,
    blueCounter = 0;

function resetVar(){
    userscore = 0,
    red = 0,
    green = 0,
    blue = 0,
    colorBoost = [50, 200, 400, 500, 1000],
    colorBoostBonus = [2, 5, 10, 15, 100],
    multiplicator = 0.5,
    listBoost = [0, 0, 0],
    victory = false,
    redStop = false,
    greenStop = false,
    blueStop = false,
    redCounter = 0,
    greenCounter = 0,
    blueCounter = 0;
}

const MAXCOUNTER = 10;

app.use(express.static('public'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
    // Initialisation des paramètres actifs
    resetVar();
    socket.emit('newScore', userscore);
    socket.emit('newBoost', [listBoost, colorBoost, multiplicator]);

    /**
     * Amélioration du score et de la couleur rouge
     */
    socket.on('clickR', function () {
        userscore += colorBoostBonus[listBoost[0]];
        red += colorBoostBonus[listBoost[0]];
        if (red >= 255) {
            red = 255;
            if(!redStop){
                redStop = true;
                var redTimer = setInterval(function () {
                    socket.emit('freeze', ['red', MAXCOUNTER - redCounter]);
                    redCounter++;
                    if(victory){
                        clearInterval(redTimer);
                    }
                    if (redCounter > MAXCOUNTER) {
                        redCounter = 0;
                        red = 0;
                        redStop = false;
                        clearInterval(redTimer);
                    }
                }, 1000);
            }
        }
        checkVictory();
        if (!victory) {
            socket.emit('newScore', userscore);
            socket.emit('newColor', [red, green, blue]);
        }
    });

    /**
     * Amélioration du score et de la couleur vert
     */
    socket.on('clickG', function () {
        userscore += colorBoostBonus[listBoost[1]];
        green += colorBoostBonus[listBoost[1]];
        if (green >= 255) {
            green = 255;
            if(!greenStop) {
                greenStop = true;
                var greenTimer = setInterval(function () {
                    socket.emit('freeze', ['green', MAXCOUNTER - greenCounter]);
                    greenCounter++;
                    if (victory) {
                        clearInterval(greenTimer);
                    }
                    if (greenCounter > MAXCOUNTER) {
                        greenCounter = 0;
                        green = 0;
                        greenStop = false;
                        clearInterval(greenTimer);
                    }
                }, 1000);
            }
        }
        checkVictory();
        if (!victory) {
            socket.emit('newScore', userscore);
            socket.emit('newColor', [red, green, blue]);
        }
    });

    /**
     * Amélioration du score et de la couleur bleu
     */
    socket.on('clickB', function () {
        userscore += colorBoostBonus[listBoost[2]];
        blue += colorBoostBonus[listBoost[2]];
        if (blue >= 255) {
            blue = 255;
            if(!blueStop) {
                blueStop = true;
                var blueTimer = setInterval(function () {
                    socket.emit('freeze', ['blue', MAXCOUNTER - blueCounter]);
                    blueCounter++;
                    if (victory) {
                        clearInterval(blueTimer);
                    }
                    if (blueCounter > MAXCOUNTER) {
                        blueCounter = 0;
                        blue = 0;
                        blueStop = false;
                        clearInterval(blueTimer);
                    }
                }, 1000);
            }
        }
        checkVictory();
        if (!victory) {
            socket.emit('newScore', userscore);
            socket.emit('newColor', [red, green, blue]);
        }
    });

    socket.on('clickBoostR', function () {
        upgradeBoost(0);
    });

    socket.on('clickBoostG', function () {
        upgradeBoost(1);
    });

    socket.on('clickBoostB', function () {
        upgradeBoost(2);
    });

    /**
     * Réduction des couleurs toutes les secondes
     */
    setInterval(function () {
        if (!victory) {
            if (!redStop) {
                red -= 1;
            }

            if (!greenStop) {
                green -= 1;
            }

            if (!blueStop) {
                blue -= 1;
            }

            if (red < 0) {
                red = 0;
            }
            if (green < 0) {
                green = 0;
            }
            if (blue < 0) {
                blue = 0;
            }
            socket.emit('newColor', [red, green, blue]);
        }
    }, 100);

    function upgradeBoost(i) {
        var boost = listBoost[i];
        var requiredScore = colorBoost[boost] * (multiplicator + boost);
        if (i < colorBoost.length && userscore >= requiredScore) {
            listBoost[i]++;
            userscore -= requiredScore;
            socket.emit('newScore', userscore);
            socket.emit('newBoost', [listBoost, colorBoost, multiplicator]);
        }
    }

    function checkVictory() {
        if (red >= 255 && green >= 255 && blue >= 255) {
            socket.emit('victory');
            victory = true;
        }
    }
});

http.listen(3000, function () {
    console.log('listening on *:3000');
});